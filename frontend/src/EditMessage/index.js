import React from "react";

import './EditMessage.css';
import {Button, Segment, TextArea} from "semantic-ui-react";
import {setNewMessageId, hideEdit} from "./actions";
import {updateMessage} from '../Messages/actions'
import {connect} from "react-redux";
import {focusEnd} from "../helperFunctions/textArea";
import {withRouter} from "react-router";

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ""
        }
        this.onChange = this.onChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onKey = this.onKey.bind(this);
    }

    onChange(event) {
        this.setState({
            value: event.target.value
        });
    }

    onCancel() {
        this.props.setNewMessageId(null);
        this.props.history.push("/");
    }

    onSave() {
        if (this.state.value !== "") {
            this.props.updateMessage(this.props.messageId, this.state.value);
            this.props.setNewMessageId(null);
            this.props.history.push("/");
        }
    }

    onKey(event) {
        if (event.keyCode === 27) { // Escape
            this.onCancel();
            event.preventDefault();
        }
    }

    componentDidMount() {
        document.addEventListener("keydown", this.onKey, false);

        if (this.props.messageId) {
            const message = this.props.messages.find((message) => message.id === this.props.match.params.id);
            this.setState({value: message ? message.text : ""});
        }
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.onKey, false);
    }


    render() {
        return (
            <div className={"EditContainer"}>
                <Segment className={"EditMessage"}>
                    <TextArea className="TextArea"
                              id="EditArea"
                              value={this.state.value}
                              onChange={this.onChange}/>
                    <Button.Group>
                        <Button basic onClick={this.onCancel}>Cancel</Button>
                        <Button onClick={this.onSave}>Save</Button>
                    </Button.Group>
                </Segment>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    messageId: state.messageForm.messageId,
    messages: state.messages.data
});

const mapDispatchToProps = {
    setNewMessageId,
    updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditMessage));