import {SET_NEW_MESSAGE_ID, HIDE_PAGE, SHOW_PAGE} from './actionTypes';

export const setNewMessageId = (id) => ({
    type: SET_NEW_MESSAGE_ID,
    payload: {
        messageId: id
    }
});
