import {applyMiddleware, compose, createStore} from "redux";
import rootReducer from "./reducers";
import {createBrowserHistory} from 'history';
import createSagaMiddleware from 'redux-saga';
import rootSaga from "./sagas";

// export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

export default createStore(
    rootReducer,
    compose(applyMiddleware(sagaMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ));

sagaMiddleware.run(rootSaga);
