import {Route, Switch} from "react-router-dom";

import React from "react";
import LoginPage from "../LoginPage";
import EditMessage from "../EditMessage";
import Messages from "../Messages";
import UserList from "../UserList";

const Routing = ({isLoading}) => {
    return (
        (
            <main>
                <Switch>
                    <Route exact path="/" component={Messages}/>
                    <Route exact path="/login" component={LoginPage}/>
                    <Route exact path="/users" component={UserList}/>
                    <Route path = "/:id" component={EditMessage}/>
                    {/*<Route path="/user/:id" component={UserPage}*/}
                </Switch>
            </main>
        )
    );
}


export default Routing;
