import axios from 'axios';
import {v4 as uuidv4} from "uuid";

export function getAllMessages() {
    return axios.get("http://localhost:8080/api/messages");
}

export function sendMessage(text, user) {
    const newMessage = {
        id: uuidv4(),
        userId: user.id,
        text: text,
        createdAt: new Date().toJSON(),
        editedAt: ""
    };

    return axios.post("http://localhost:8080/api/messages/new", newMessage)
}

export function editMessage(id, newText) {
    const request = {
        id: id,
        text: newText,
        editedAt: new Date().toJSON()
    }
    return axios.patch("http://localhost:8080/api/messages/edit", request);

}

export function deleteMessage(id) {
    return axios.delete("http://localhost:8080/api/messages/delete/" + id);
}