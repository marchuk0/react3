import store, {history} from "../store";
import Routing from "../Routing";
import {Provider} from "react-redux";
import React from "react";
import {Route, BrowserRouter as Router} from "react-router-dom";

const Home = () => (
    <Provider store={store}>
        <Router >
            {/*<Routing />*/}
            <Route path="/" component={Routing}/>
        </Router>
    </Provider>
);

export default Home;