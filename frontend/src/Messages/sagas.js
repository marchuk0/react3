import {
    FETCH_MESSAGES,
    FETCH_MESSAGES_LOADING,
    FETCH_MESSAGES_SUCCESS,
    FETCH_MESSAGES_ERROR,
    CREATE_MESSAGE,
    UPDATE_MESSAGE,
    DELETE_MESSAGE,
    CHANGE_LIKE
} from "./actionTypes";
import {all, takeEvery, call, put} from "redux-saga/effects";
import {getAllMessages, sendMessage, deleteMessage, editMessage} from "../services/messagesService"


function* fetchMessage() {
    yield put({type: FETCH_MESSAGES_LOADING});
    try {
        const response = yield call(getAllMessages);
        yield put({type: FETCH_MESSAGES_SUCCESS, payload: response.data});
        console.log(response);
    } catch (error) {
        yield put({type: FETCH_MESSAGES_ERROR, error: error});
    }
}

function* watchFetchMessages() {
    yield takeEvery(FETCH_MESSAGES, fetchMessage);
}

function* createMessageSaga(action) {
    const text = action.payload.text;
    const user = action.payload.user;
    try {
        yield call(sendMessage, text, user);
        yield put({type: FETCH_MESSAGES});
    } catch (error) {
        console.log(error);
    }
}

function* watchCreateMessageSaga() {
    yield takeEvery(CREATE_MESSAGE, createMessageSaga);
}

function* editMessageSaga(action) {
    console.log("EDITING MESSAGE IN SAGA")
    const id = action.payload.id;
    const newText = action.payload.newText;
    try {
        yield call(editMessage, id, newText);
        yield put({type: FETCH_MESSAGES});
    } catch (error) {
        console.log(error);
    }
}

function* watchEditMessageSaga() {
    yield takeEvery(UPDATE_MESSAGE, editMessageSaga);
}

function* deleteMessageSaga(action) {
    const id = action.payload.id;
    console.log(id);
    try{
        yield call(deleteMessage, id);
        yield put({type: FETCH_MESSAGES});
    } catch (error) {
        console.log(error);
    }
}

function* watchDeleteMessageSaga() {
    yield takeEvery(DELETE_MESSAGE, deleteMessageSaga)
}

export default function* messageSagas() {
    yield all([
        watchFetchMessages(),
        watchCreateMessageSaga(),
        watchEditMessageSaga(),
        watchDeleteMessageSaga()
    ]);
}