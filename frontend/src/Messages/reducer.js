import {
    FETCH_MESSAGES_LOADING,
    FETCH_MESSAGES_SUCCESS,
    FETCH_MESSAGES_ERROR,
    CHANGE_LIKE,
    DELETE_MESSAGE,
    UPDATE_MESSAGE,
    CREATE_MESSAGE
} from "./actionTypes";
import {v4 as uuidv4} from 'uuid';

const initialState = {
    data: null,
    loading: false,
    error: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGES_LOADING: {
            return {
                ...state,
                loading: true
            }
        }
        case FETCH_MESSAGES_SUCCESS: {
            return {
                data: action.payload,
                loading: false,
                error: null
            }
        }
        case FETCH_MESSAGES_ERROR: {
            return {
                ...state,
                loading: false,
                error: action.error
            }
        }
        // case CHANGE_LIKE: {
        //     const id = action.payload.id;
        //     return state.map((message) => {
        //         if (message.id === id) {
        //             return {
        //                 ...message,
        //                 isLiked: !message.isLiked
        //             }
        //         } else {
        //             return message;
        //         }
        //     });
        // }

        default:
            return state;
    }
}