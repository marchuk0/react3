import React from 'react';
import Header from "./components/Header/Header";
import Spinner from "./components/Spinner/Spinner";
import SendMessageForm from "./components/SendMessageForm/SendMessageForm";
import MessageList from './components/MessageList/MessageList';

import {connect} from "react-redux";
import {fetchMessages} from './actions'
import './index.css';
import { withRouter} from "react-router";


class Messages extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if(!this.props.user) {
            this.props.history.push("/login")
            return;
        }
        if(!this.props.messages)
        {
            this.props.fetchMessages();
        }

    }

    render() {
        if (!this.props.messages) {
            return (<Spinner/>);
        }
        const name = "MyChat";
        const usersCount = 12;
        const messagesCount = 0;
        const lastDate = messagesCount === 0 ? null : this.props.messages[messagesCount - 1].createdAt;

        return (
            <>
                <Header
                    chatName={name}
                    usersCount={usersCount}
                    messagesCount={messagesCount}
                    lastDate={lastDate}/>

                <MessageList />

                <SendMessageForm user={this.props.user}/>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages.data,
        isLoading: state.messages.loading,
        user: state.login.user,
    }
};

const mapDispatchToProps = {
    fetchMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Messages));
