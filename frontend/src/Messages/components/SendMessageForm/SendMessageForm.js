import React from "react";
import {Button, Segment, TextArea} from "semantic-ui-react";
import './SendMessageForm.css'
import {createMessage} from '../../actions';
import {connect} from "react-redux";
import PropTypes from 'prop-types';

class SendMessageForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ""};
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onKey = this.onKey.bind(this);
    }

    onChange(event) {
        this.setState({value: event.target.value});
    }

    onSubmit(event) {
        if (this.state.value.trim() === "") {
            return;
        }
        this.props.createMessage(this.props.user, this.state.value);
        this.setState({value: ""});
    }

    onKey(event) {
        if (event.ctrlKey && event.key === 'Enter') {
            this.setState({value: this.state.value + "\n"})
            event.preventDefault();
            return;
        }
        if (event.key === 'Enter' && !this.props.isShown) {
            this.onSubmit();
            event.preventDefault();
        }


    }

    componentDidMount() {
        document.addEventListener("keydown", this.onKey, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.onKey, false);
    }


    render() {
        return (
            <div className="FormContainer">
                <Segment className="SendMessageForm">
                    <TextArea className="TextArea"
                              id="WriteArea"
                              value={this.state.value}
                              onChange={this.onChange}
                              placeholder="Write a message..."/>
                    <div>
                        <Button floated={"right"} onClick={this.onSubmit}>
                            Send Message
                        </Button>
                    </div>
                </Segment>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    isShown: state.messageForm.isShown
});

const mapDispatchToProps = {
    createMessage
}

SendMessageForm.propTypes = {
    user: PropTypes.object.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(SendMessageForm);