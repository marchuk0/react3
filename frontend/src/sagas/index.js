import {all} from "redux-saga/effects";
import loginSagas from "../LoginPage/sagas";
import messageSagas from "../Messages/sagas";

export default function* rootSaga() {
    yield all([
        loginSagas(),
        messageSagas()
    ]);
}