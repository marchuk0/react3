import {
    LOGIN_USER,
    LOGIN_USER_ERROR,
    LOGIN_USER_LOADING,
    LOGIN_USER_SUCCESS
} from "./actionTypes"
import {all, takeEvery, put, call} from "redux-saga/effects";
import axios from "axios";


export function* loginUser(action) {
    yield put({type: LOGIN_USER_LOADING});
    const requestParam = action.payload;
    try {
        const user = yield call(
            axios.post,
            "http://localhost:8080/api/login",
            requestParam
        );
        // console.log(user);
        yield put({type: LOGIN_USER_SUCCESS, payload: user.data});
    } catch (error) {
        yield put({type: LOGIN_USER_ERROR, error});
    }
}

function* watchUserLogin() {
    yield takeEvery(LOGIN_USER, loginUser);
}

export default function* loginSagas() {
    yield all([
        watchUserLogin()
    ]);
}