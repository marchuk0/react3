import React, {useState} from "react";
import {Button, Form, Message, Segment} from "semantic-ui-react";
import validator from "validator";
import PropTypes from "prop-types";
import {login} from "../actions";
import {connect} from "react-redux";

const LoginForm = ({login, isLoading, error}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isEmailValid, setIsEmailValid] = useState(true);
    const [isPasswordValid, setIsPasswordValid] = useState(true);

    const emailChanged = data => {
        setEmail(data);
        setIsEmailValid(true);
    };

    const passwordChanged = data => {
        setPassword(data);
        setIsPasswordValid(true);
    };

    const handleLoginClick = async () => {
        const isValid = isEmailValid && isPasswordValid;
        if (!isValid || isLoading) {
            return;
        }
        login(email, password);
    };

    return (
        <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
            <Segment>
                <Form.Input
                    fluid
                    icon="at"
                    iconPosition="left"
                    placeholder="Email"
                    type="username"
                    error={!isEmailValid}
                    onChange={ev => emailChanged(ev.target.value)}
                    onBlur={() => setIsEmailValid(Boolean(email))}
                />
                <Form.Input
                    fluid
                    icon="lock"
                    iconPosition="left"
                    placeholder="Password"
                    type="password"
                    error={!isPasswordValid}
                    onChange={ev => passwordChanged(ev.target.value)}
                    onBlur={() => setIsPasswordValid(Boolean(password))}
                />
                <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
                    Login
                </Button>
            </Segment>

            {error ? (<Message color={"red"}>"Incorrect email or password"</Message>): null}
        </Form>
    );
};

LoginForm.propTypes = {
    login: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string
};

const mapStateToProps = (state) => ({
    isLoading: state.login.loading,
    error: state.login.error
});

const mapDispatchToProps = {
    login
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);