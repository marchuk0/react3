import {Grid, Header} from "semantic-ui-react";
import React, {useEffect} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types"
import LoginForm from "./components/LoginForm";
import {withRouter} from "react-router";

const LoginPage = ({user, history}) => {

    useEffect(() => {
        if(user && (user.email === "admin")) {
            history.push("/users");
        }
        else if(user) {
            history.push("/");
        }

    });

    return (
        <Grid textAlign="center" verticalAlign="middle" className="fill">
            <Grid.Column style={{maxWidth: 450}}>
                <Header as="h2" color="teal" textAlign="center">
                    Login to your account
                </Header>
                <LoginForm/>
            </Grid.Column>
        </Grid>
    );
}


const mapStateToProps = (state) => ({
    user: state.login.user
});

export default connect(mapStateToProps)(withRouter(LoginPage));