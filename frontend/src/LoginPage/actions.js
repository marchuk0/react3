import {LOGIN_USER} from './actionTypes';

export const login = (email, password) => ({
    type: LOGIN_USER,
    payload: {
        email,
        password
    }
});