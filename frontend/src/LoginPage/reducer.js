import {LOGIN_USER_SUCCESS, LOGIN_USER_ERROR, LOGIN_USER_LOADING} from "./actionTypes";

const initialState = {
    user: null,
    loading: false,
    error: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN_USER_LOADING: {
            return {
                user: null,
                loading: true,
                error: null
            }
        }
        case LOGIN_USER_SUCCESS: {
            return {
                user: action.payload,
                loading: false,
                error: null
            }
        }
        case LOGIN_USER_ERROR: {
            return {
                user: null,
                loading: false,
                error: action.error
            }
        }
        default:
            return state;
    }
}