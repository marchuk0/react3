import {combineReducers} from "redux";
import messages from '../Messages/reducer';
import messageForm from '../EditMessage/reducer';
import login from '../LoginPage/reducer';
import {createBrowserHistory} from "history";

const rootReducer = combineReducers({
    messages,
    messageForm,
    login
})

export default rootReducer;