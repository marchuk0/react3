package com.bsa.auth.dto;

import com.bsa.auth.User;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class UserListItemDto {
    private UUID id;
    private String email;
    private String password;
    private String username;
    private String avatar;

    public static UserListItemDto fromEntity(User user) {
        return UserListItemDto
                .builder()
                .id(user.getId())
                .email(user.getEmail())
                .username(user.getUserName())
                .avatar(user.getAvatar())
                .build();
    }
}
