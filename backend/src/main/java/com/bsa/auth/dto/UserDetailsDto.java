package com.bsa.auth.dto;

import com.bsa.auth.User;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class UserDetailsDto {
    private UUID id;
    private String email;
    private String username;
    private String avatar;

    public static UserDetailsDto fromEntity(User user) {
        return UserDetailsDto
                .builder()
                .id(user.getId())
                .email(user.getEmail())
                .username(user.getUserName())
                .avatar(user.getAvatar())
                .build();
    }
}
