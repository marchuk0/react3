package com.bsa.auth;

import com.bsa.auth.dto.UserDetailsDto;
import com.bsa.auth.dto.UserListItemDto;
import com.bsa.auth.dto.UserLoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AuthController {

    @Autowired
    AuthService authService;

    @GetMapping("/login/all")
    public List<UserDetailsDto> getAll() {
        return authService.getAll();
    }

    @PostMapping("/login")
    public UserDetailsDto login(@RequestBody UserLoginDto userLoginDto) throws Exception {
        return authService.login(userLoginDto);
    }

    @GetMapping("/users/all")
    public List<UserListItemDto> getUserList() {
        return authService.getAllUsers();
    }

    @GetMapping("/users/{id}")
    public UserListItemDto getUser(@PathVariable UUID id) {
        return authService.getUserById(id);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable UUID id) {
        authService.deleteUser(id);
    }

    @PostMapping("/users/new")
    public void addUser(@RequestBody UserListItemDto userListItemDto) {
        authService.addUser(userListItemDto);
    }

    @PatchMapping("/users/edit")
    public void editUser(@RequestBody UserListItemDto userListItemDto) {
        authService.addUser(userListItemDto);
    }

}
