package com.bsa.auth;

import com.bsa.auth.dto.UserDetailsDto;
import com.bsa.auth.dto.UserListItemDto;
import com.bsa.auth.dto.UserLoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;


    public List<UserDetailsDto> getAll() {
        return userRepository.findAll().stream().map(UserDetailsDto::fromEntity).collect(Collectors.toList());
    }

    public UserDetailsDto login(UserLoginDto userLoginDto) throws Exception {
        if(userLoginDto.getEmail().equals("admin") &&  userLoginDto.getPassword().equals("admin"))
        {
            return UserDetailsDto.builder()
                    .username("admin")
                    .email("admin")
                    .avatar("")
                    .id(UUID.randomUUID())
                    .build();
        }
        var user = userRepository
                .getFirstByEmailAndPassword(userLoginDto.getEmail(), userLoginDto.getPassword())
                .orElseThrow();
        return UserDetailsDto.fromEntity(user);

    }

    public List<UserListItemDto> getAllUsers() {
        return userRepository.findAll().stream().map(UserListItemDto::fromEntity).collect(Collectors.toList());
    }

    public UserListItemDto getUserById(UUID id) {
        return UserListItemDto.fromEntity(userRepository.findById(id).orElseThrow());
    }

    public void deleteUser(UUID id) {
        userRepository.deleteById(id);
    }

    public void addUser(UserListItemDto userListItemDto) {
        User user = User.builder()
                .email(userListItemDto.getEmail())
                .password(userListItemDto.getPassword())
                .userName(userListItemDto.getUsername())
                .avatar(userListItemDto.getAvatar())
                .id(userListItemDto.getId())
                .build();
        userRepository.save(user);
    }
}
