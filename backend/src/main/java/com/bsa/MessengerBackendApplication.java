package com.bsa;

import com.bsa.auth.User;
import com.bsa.auth.UserRepository;
import com.bsa.messages.Message;
import com.bsa.messages.MessageRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.UUID;

@SpringBootApplication
public class MessengerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessengerBackendApplication.class, args);
	}

//	@Bean
//	CommandLineRunner initDB(UserRepository userRepository, MessageRepository messageRepository) {
//		return args -> {
//
//		};
//	}
}
