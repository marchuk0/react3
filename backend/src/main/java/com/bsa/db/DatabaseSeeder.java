package com.bsa.db;

import com.bsa.auth.User;
import com.bsa.auth.UserRepository;
import com.bsa.messages.Message;
import com.bsa.messages.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DatabaseSeeder {

    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserRepository userRepository;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        var user1 = User.builder()
                .email("hello@gmail.com")
                .password("1")
                .id(UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"))
                .avatar("https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA")
                .userName("Ruth")
                .build();

        userRepository.save(user1);

        var user2 = User.builder()
                .email("wif@gmail.com")
                .password("hdhd")
                .id(UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"))
                .avatar("https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng")
                .userName("Wendy")
                .build();

        userRepository.save(user2);

        var message1 = Message.builder()
                .id(UUID.fromString("80f08600-1b8f-11e8-9629-c7eca82aa7bd"))
                .text("I don’t *** understand. It's the Panama accounts")
                .createdAt("2020-07-16T19:48:12.936Z")
                .editedAt("")
                .user(user1)
                .build();

        messageRepository.save(message1);

        var message2 = Message.builder()
                .id(UUID.fromString("80e00b40-1b8f-11e8-9629-c7eca82aa7bd"))
                .text("Tells exactly what happened.")
                .createdAt("2020-07-16T19:48:42.481Z")
                .editedAt("2020-07-16T19:48:47.481Z")
                .user(user2)
                .build();

        messageRepository.save(message2);


    }
}
