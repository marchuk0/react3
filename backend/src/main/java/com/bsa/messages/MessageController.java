package com.bsa.messages;

import com.bsa.messages.dto.MessageDto;
import com.bsa.messages.dto.MessageEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class MessageController {

    @Autowired
    MessageService messageService;

    @GetMapping("/messages")
    public List<MessageDto> getAllMessages() {
        return messageService.getAll();
    }

    @PostMapping("/messages/new")
    public void newMessage(@RequestBody MessageDto messageDto) {
        messageService.newMessage(messageDto);
    }

    @PatchMapping("/messages/edit")
    public void newMessage(@RequestBody MessageEditDto messageEditDto){
        messageService.edit(messageEditDto);
    }

    @DeleteMapping("/messages/delete/{id}")
    public void deleteMessage(@PathVariable UUID id) {
        messageService.delete(id);
    }
}
