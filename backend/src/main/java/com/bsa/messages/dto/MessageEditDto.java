package com.bsa.messages.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class MessageEditDto {
    private UUID id;
    private String text;
    private String editedAt;
}
