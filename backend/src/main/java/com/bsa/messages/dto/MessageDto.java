package com.bsa.messages.dto;

import com.bsa.messages.Message;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class MessageDto {
    private UUID id;
    private UUID userId;
    private String avatar;
    private String user;
    private String text;
    private String createdAt;
    private String editedAt;

    public static MessageDto fromEntity(Message message) {
        return MessageDto
                .builder()
                .id(message.getId())
                .userId(message.getUser().getId())
                .avatar(message.getUser().getAvatar())
                .user(message.getUser().getUserName())
                .text(message.getText())
                .createdAt(message.getCreatedAt())
                .editedAt(message.getEditedAt())
                .build();
    }
}
