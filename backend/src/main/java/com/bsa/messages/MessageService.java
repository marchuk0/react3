package com.bsa.messages;

import com.bsa.auth.User;
import com.bsa.auth.UserRepository;
import com.bsa.messages.dto.MessageDto;
import com.bsa.messages.dto.MessageEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessageService {

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    UserRepository userRepository;

    public List<MessageDto> getAll() {
        return messageRepository.findAll()
                .stream()
                .map(MessageDto::fromEntity)
                .collect(Collectors.toList());
    }

    public void newMessage(MessageDto messageDto) {
        Message message = new Message();
        User user = userRepository.findById(messageDto.getUserId()).orElseThrow();
        message.setId(messageDto.getId());
        message.setText(messageDto.getText());
        message.setCreatedAt(messageDto.getCreatedAt());
        message.setEditedAt(messageDto.getEditedAt());
        message.setUser(user);
        messageRepository.save(message);
    }

    public void edit(MessageEditDto messageEditDto) {
        Message message = messageRepository.findById(messageEditDto.getId()).orElseThrow();
        message.setText(messageEditDto.getText());
        message.setEditedAt(messageEditDto.getEditedAt());
        messageRepository.save(message);
    }

    public void delete(UUID id) {
        messageRepository.deleteById(id);
    }
}
